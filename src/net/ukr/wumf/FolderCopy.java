package net.ukr.wumf;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class FolderCopy {

	public void copyFolder(String folderInName, String folderOutName)
			throws FileNotFoundException, InterruptedException {
		File folderIn = new File(folderInName);
		File folderOut = buildOutFolder(folderOutName);
		File[] filesIn = folderIn.listFiles();
		checkParametrs(folderInName, filesIn);
		List<Thread> threads = buildExecutors(filesIn, folderOut);
		runExecutors(threads);
	}

	private static File buildOutFolder(String folderOutName) {
		File folderOut = new File(folderOutName);
		folderOut.mkdir();
		return folderOut;
	}

	private static void checkParametrs(String folderInName, File[] filesIn) throws FileNotFoundException {
		if (filesIn == null) {
			throw new FileNotFoundException(folderInName);
		}
	}

	private List<Thread> buildExecutors(File[] filesIn, File folderOut) {
		List<Thread> threads = new ArrayList<>();
		for (File fileIn : filesIn) {
			if (fileIn.isFile()) {
				File fileOut = new File(folderOut.getAbsolutePath() + File.separator + fileIn.getName());
				FileCopy fs = new FileCopy(fileIn, fileOut);
				Thread thread = new Thread(fs);
				threads.add(thread);
			}
		}
		return threads;
	}

	private void runExecutors(List<Thread> threads) throws InterruptedException {
		for (Thread thread : threads) {
			thread.start();
		}
		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
