package net.ukr.wumf;

import java.io.FileNotFoundException;

public class Main {

	public static void main(String[] args) throws FileNotFoundException, InterruptedException {
		String f1 = "folderOne";
		String f2 = "folderTwo";

		FolderCopy copyService = new FolderCopy();
		copyService.copyFolder(f1, f2);
	}
}