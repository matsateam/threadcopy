package net.ukr.wumf;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileCopy implements Runnable {
	private File in;
	private File out;

	public FileCopy(File in, File out) {
		super();
		this.in = in;
		this.out = out;
	}

	@Override
	public void run() {
		try (InputStream is = new FileInputStream(in);
				OutputStream os = new FileOutputStream(out);
				BufferedInputStream bi = new BufferedInputStream(is);
				BufferedOutputStream bo = new BufferedOutputStream(os)) {
			bi.transferTo(bo);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}